﻿namespace TodoList.Entities
{
    public class Task: Entity
    {
        public int ListId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public bool IsComplete { get; set; }
    }
}
