﻿using System;
using TodoList.Entities;
using TodoList.Services;

namespace Todo_List.ConsoleApp
{
    class UserConsoleUtilities
    {
        internal static void PrintAllUsers()
        {
            Console.WriteLine();
            foreach (var user in UserService.Instance.AllUsers)
            {
                Console.WriteLine($"Id: {user.Id}; Username: {user.Username}; First name: {user.FirstName}; Last name: {user.LastName}; Role: {user.Role}");
            }
        }

        internal static bool CreateUser()
        {
            // ask for parameters
            Console.Write("Please enter the username: ");
            string username = Console.ReadLine();
            bool isUserExists = UserService.Instance.IsUsernameUsed(username);
            if (isUserExists)
            {
                return false;
            }

            Console.Write("Enter Password: ");
            string password = Console.ReadLine();
            Console.Write("Enter First Name: ");
            string firstName = Console.ReadLine();
            Console.Write("Enter Last Name: ");
            string lastName = Console.ReadLine();

            string roleString = "";
            while (true)
            {
                Console.WriteLine("Enter Role (1 for Admin, 2 for Regular User): ");
                roleString = Console.ReadLine();
                if (roleString == "1" || roleString == "2")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Please enter a valid input!");
                }
            }

            UserRole role = roleString == "1" ? UserRole.Admin : UserRole.RegularUser;

            // CreateUser user
            UserService.Instance.CreateUser(username, password, firstName, lastName, role);
            return true;
        }

        internal static bool DeleteUser()
        {
            Console.Write("Please enter the ID of the user to delete:");
            int id = 0;
            while (true)
            {
                try
                {
                    id = int.Parse(Console.ReadLine());
                    break;
                }
                catch (ArgumentException)
                {
                    Console.WriteLine("Please enter a valid number!");
                }
                
            }

            if (!UserService.Instance.IdExists(id))
            {
                return false;
            }

            UserService.Instance.DeleteUser(id);
            return true;
        }

        /// <summary>
        /// Reads the necessary information from the console and updates the User object through the 
        /// UserService.
        /// </summary>
        /// <returns>True when the user exists and is updated. Otherwise - false.</returns>
        internal static bool UpdateUser()
        {
            Console.Write("Please enter the ID of the user to update:");
            int id = 0;
            while (true)
            {
                try
                {
                    id = int.Parse(Console.ReadLine());
                    break;
                }
                catch (ArgumentException)
                {
                    Console.WriteLine("Please enter a valid number!");
                }

            }

            if (!UserService.Instance.IdExists(id))
            {
                return false;
            }

            Console.Write("Please enter the username: ");
            string username = Console.ReadLine();
            Console.Write("Enter Password: ");
            string password = Console.ReadLine();
            Console.Write("Enter First Name: ");
            string firstName = Console.ReadLine();
            Console.Write("Enter Last Name: ");
            string lastName = Console.ReadLine();

            UserService.Instance.UpdateUser(id, username, password, firstName, lastName);
                
            return true;
        }
    }
}
