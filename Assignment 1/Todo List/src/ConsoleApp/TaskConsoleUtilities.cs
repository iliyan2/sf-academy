﻿using System;
using System.Collections.Generic;
using TodoList.Entities;
using TodoList.Services;

namespace TodoList.ConsoleApp
{
    class TaskConsoleUtilities
    {
        internal static void PrintAllTasks()
        {
            Console.Write("Please enter the ToDo list Id: ");
            int listId = 0;
            while (true)
            {
                try
                {
                    listId = int.Parse(Console.ReadLine());
                    break;
                }
                catch (ArgumentException)
                {
                    Console.WriteLine("Please enter a valid number!");
                }
                
            }

            Console.WriteLine();
            List<Task> tasksByListId = TaskService.Instance.GetTasksForList(listId);

            foreach (var task in tasksByListId)
            {
                Console.WriteLine($"Id: {task.Id}\nTitle: {task.Title}\nDescription: {task.Description}\nCompletion status: {task.IsComplete}");
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        internal static bool CreateTask()
        {
            // ask for parameters
            Console.Write("Please enter the task title: ");
            string taskTitle = Console.ReadLine();

            bool titleExists = TaskService.Instance.IsTitleUsed(taskTitle);
            if (titleExists)
            {
                return false;
            }

            Console.Write("Please enter the ToDo list Id: ");
            int toDoListId = 0;
            while (true)
            {
                try
                {
                    toDoListId = int.Parse(Console.ReadLine());
                    break;
                }
                catch (ArgumentException)
                {
                    Console.WriteLine("Please enter a valid number!");
                }
            }

            if (!ListService.Instance.IdExists(toDoListId))
            {
                Console.WriteLine($"There is no ToDo List with Id={toDoListId}!");
                return false;
            }

            int createdById = UserService.Instance.CurrentUser.Id;

            Console.Write("Please enter a description of the task: ");
            string description = Console.ReadLine();

            Console.Write("Please enter if the task is comleted Y/N: ");
            bool isComplete = false;
            while (true)
            {
                string isCompleteStr = Console.ReadLine();
                if (isCompleteStr == "Y" || isCompleteStr == "N")
                {
                    isComplete = isCompleteStr == "Y" ? true : false;
                    break;
                }
                else
                {
                    Console.WriteLine("Please enter a valid value: Y or N");
                }
            }

            TaskService.Instance.CreateTaskInList(toDoListId, taskTitle, createdById, description, isComplete);

            return true;
        }

        /// <summary>
        /// Reads the necessary information from the console and updates the ToDoList object through the 
        /// ListService.
        /// </summary>
        /// <returns>True when the ToDoList exists and is updated. Otherwise - false.</returns>
        internal static bool UpdateTask()
        {
            Console.Write("Please enter the ID of the task to update:");
            int id = 0;
            while (true)
            {
                try
                {
                    id = int.Parse(Console.ReadLine());
                    break;
                }
                catch (ArgumentException)
                {
                    Console.WriteLine("Please enter a valid number!");
                }
            }

            if (!TaskService.Instance.IdExists(id))
            {
                return false;
            }

            Console.Write("Please enter the new title of the task: ");
            string title = Console.ReadLine();

            Console.Write("Please enter the new description: ");
            string description = Console.ReadLine();

            Console.Write("Please enter if the task is comleted Y/N: ");
            bool isComplete = false;
            while (true)
            {
                string isCompleteStr = Console.ReadLine();
                if (isCompleteStr == "Y" || isCompleteStr == "N")
                {
                    isComplete = isCompleteStr == "Y" ? true : false;
                    break;
                }
                else
                {
                    Console.WriteLine("Please enter a valid value: Y or N");
                }
            }

            int editedById = UserService.Instance.CurrentUser.Id;

            TaskService.Instance.UpdateTask(id, title, description, isComplete, editedById);

            return true;
        }

        internal static bool DeleteTask()
        {
            Console.Write("Please enter the id of the task to delete: ");
            int id = 0;
            while (true)
            {
                try
                {
                    id = int.Parse(Console.ReadLine());
                    break;
                }
                catch (ArgumentException)
                {
                    Console.WriteLine("Please enter a valid number!");
                }
            }

            if (!TaskService.Instance.IdExists(id))
            {
                return false;
            }

            TaskService.Instance.DeleteTask(id);
            return true;
        }
    }
}
