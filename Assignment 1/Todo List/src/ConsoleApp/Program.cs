﻿using System;
using Todo_List.ConsoleApp;
using TodoList.ConsoleApp;
using TodoList.Entities;
using TodoList.Services;

namespace TodoList
{
    class Program
    {
        private static UserService _userService = null;

        static void Main(string[] args)
        {
            _userService = UserService.Instance;
            if (args.Length > 0)
            {
                _userService.Login(args[0], args[1]);
            }
            bool shouldExit = false;
            while (!shouldExit)
            {
                shouldExit = ShowMainMenu();
            }
        }

        private static bool ShowMainMenu()
        {
            RenderMenu();

            string userChoice = Console.ReadLine();

            switch (userChoice)
            {
                case "1":
                    if (_userService.CurrentUser == null)
                    {
                        LogIn();
                    }
                    else
                    {
                        LogOut();
                    }
                    return false;
                case "2":
                    ShowSubMenu(SubMenuType.List);
                    return false;
                case "3":
                    ShowSubMenu(SubMenuType.Task);
                    return false;
                case "4":
                    ShowSubMenu(SubMenuType.User);
                    return false;
                case "c":
                    Console.Clear();
                    return false;
                case "exit":
                    return true;
                default:
                    Console.WriteLine("Unknown Command");
                    return false;
            }
        }

        private static void ShowSubMenu(SubMenuType subMenuType)
        {
            bool isExitSubMenu = false;
            while (!isExitSubMenu)
            {
                RenderSubMenu(subMenuType);
                isExitSubMenu = HandleUserSelection(subMenuType);
            }
        }

        private static bool HandleUserSelection(SubMenuType subMenuType)
        {
            string userChoise = Console.ReadLine();

            switch (userChoise)
            {
                case "1":
                case "\b":
                    return true;
                case "2":
                    ShowAll(subMenuType);
                    return false;
                case "3":
                    Create(subMenuType);
                    return false;
                case "4":
                    Update(subMenuType);
                    return false;
                case "5":
                    Delete(subMenuType);
                    return false;
                case "c":
                    Console.Clear();
                    return false;
                default:
                    Console.WriteLine("Unknown command");
                    return false;
            }
        }

        private static void Update(SubMenuType subMenuType)
        {
            switch (subMenuType)
            {
                case SubMenuType.List:
                    ListConsoleUtilities.UpdateList();
                    break;
                case SubMenuType.Task:
                    TaskConsoleUtilities.UpdateTask();
                    break;
                case SubMenuType.User:
                    UserConsoleUtilities.UpdateUser();
                    break;
                default:
                    break;
            }
        }

        private static void Create(SubMenuType subMenuType)
        {
            switch (subMenuType)
            {
                case SubMenuType.List:
                    ListConsoleUtilities.CreateList();
                    break;
                case SubMenuType.Task:
                    TaskConsoleUtilities.CreateTask();
                    break;
                case SubMenuType.User:
                    UserConsoleUtilities.CreateUser();
                    break;
                default:
                    break;
            }
        }

        private static void Delete(SubMenuType subMenuType)
        {
            switch (subMenuType)
            {
                case SubMenuType.List:
                    ListConsoleUtilities.DeleteList();
                    break;
                case SubMenuType.Task:
                    TaskConsoleUtilities.DeleteTask();
                    break;
                case SubMenuType.User:
                    UserConsoleUtilities.DeleteUser();
                    break;
                default:
                    break;
            }
        }

        private static void ShowAll(SubMenuType subMenuType)
        {
            switch (subMenuType)
            {
                case SubMenuType.List:
                    ListConsoleUtilities.PrintAllLists();
                    break;
                case SubMenuType.Task:
                    TaskConsoleUtilities.PrintAllTasks();
                    break;
                case SubMenuType.User:
                    UserConsoleUtilities.PrintAllUsers();
                    break;
                default:
                    break;
            }
        }

        private static void RenderSubMenu(SubMenuType subMenuType)
        {
            string subMenuName = null;
            switch (subMenuType)
            {
                case SubMenuType.List:
                    subMenuName = "List";
                    break;
                case SubMenuType.Task:
                    subMenuName = "Task";
                    break;
                case SubMenuType.User:
                    subMenuName = "User";
                    break;
                default: // should never happens
                    break;
            }

            Console.WriteLine($"--------{subMenuName} Management Menu--------");
            Console.WriteLine("1 - Go back");
            Console.WriteLine($"2 - Show all {subMenuName}s");
            Console.WriteLine($"3 - Create a new {subMenuName}");
            Console.WriteLine($"4 - Edit a {subMenuName}");
            Console.WriteLine($"5 - Delete a {subMenuName}");
        }

        private static void LogOut()
        {
            _userService.LogOut();
        }

        private static void LogIn()
        {
            Console.WriteLine("Enter your username: ");
            string userName = Console.ReadLine();
            Console.WriteLine("Enter your password: ");
            string password = Console.ReadLine();

            _userService.Login(userName, password);
            if (_userService.CurrentUser == null)
            {
                Console.WriteLine("Login failed.");
            }
            else
            {
                Console.WriteLine("Login successful.");
            }
        }

        private static void RenderMenu()
        {
            Console.WriteLine("--------Main Menu--------");
            if (_userService.CurrentUser == null)
            {
                Console.WriteLine("1. LogIn ");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"You are logged in as: {_userService.CurrentUser.Username}");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("1. LogOut");
                Console.WriteLine("2. ToDo List Management");
                Console.WriteLine("3. Task Management");

                if (_userService.CurrentUser.Role == UserRole.Admin)
                {
                    Console.WriteLine("4. User Management");
                }
            }
        }

        public enum SubMenuType
        {
            List,
            Task,
            User
        }
    }
}
