﻿using System;
using TodoList.Services;

namespace Todo_List.ConsoleApp
{
    class ListConsoleUtilities
    {
        internal static void PrintAllLists()
        {
            Console.WriteLine();
            foreach (var list in ListService.Instance.AllToDoLists)
            {
                Console.WriteLine($"Id: {list.Id}; Title: {list.Title}");
            }
        }

        internal static bool CreateList()
        {
            // ask for parameters
            Console.Write("Please enter the list title: ");
            string listTitle = Console.ReadLine();
            bool titleExists = ListService.Instance.IsTitleUsed(listTitle);
            if (titleExists)
            {
                return false;
            }

            ListService.Instance.CreateList(listTitle, UserService.Instance.CurrentUser.Id);


            return true;
        }

        /// <summary>
        /// Reads the necessary information from the console and updates the ToDoList object through the 
        /// ListService.
        /// </summary>
        /// <returns>True when the ToDoList exists and is updated. Otherwise - false.</returns>
        internal static bool UpdateList()
        {
            Console.Write("Please enter the ID of the ToDo list to update:");
            int id = 0;
            while (true)
            {
                try
                {
                    id = int.Parse(Console.ReadLine());
                    break;
                }
                catch (ArgumentException)
                {
                    Console.WriteLine("Please enter a valid number!");
                }
            }

            if (!ListService.Instance.IdExists(id))
            {
                return false;
            }

            Console.Write("Please enter the new title: ");
            string title = Console.ReadLine();

            ListService.Instance.UpdateList(id, title, UserService.Instance.CurrentUser.Id);

            return true;
        }

        internal static bool DeleteList()
        {
            Console.Write("Please enter the id of the ToDo list to delete: ");
            int id = 0;
            while (true)
            {
                try
                {
                    id = int.Parse(Console.ReadLine());
                    break;
                }
                catch (ArgumentException)
                {
                    Console.WriteLine("Please enter a valid number!");
                }

            }

            if (!ListService.Instance.IdExists(id))
            {
                return false;
            }

            TaskService.Instance.DeleteAllTasksForList(id);
            ListService.Instance.DeleteList(id);
            return true;
        }
    }
}
