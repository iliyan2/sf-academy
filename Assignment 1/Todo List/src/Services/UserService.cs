﻿using System;
using System.Collections.Generic;
using System.Linq;
using TodoList.Data;
using TodoList.Entities;

namespace TodoList.Services
{
    /// <summary>
    /// Responsible for managing user related functionality and tracking currently logged in user
    /// </summary>
    public sealed class UserService
    {
        private static readonly UserService _instance = new UserService();

        private const string UsersStoreFileName = "Users.json";

        private readonly FileDatabase _fileDB;

        private int _userIdCount = 0;

        /// <summary>
        /// List of all application users
        /// </summary>
        private readonly List<User> _applicationUsers = new List<User>();

        static UserService() { }

        /// <summary>
        /// Initializes new instance of the UserService and creates a single default user 
        /// </summary>
        private UserService()
        {
            _fileDB = new FileDatabase();
            PersistanceWrapper<User> storedUsers = _fileDB.Read<PersistanceWrapper<User>>(UsersStoreFileName);
            
            if (storedUsers == null || storedUsers.ListToPersist == null || storedUsers.ListToPersist.Count == 0)
            {
                CreateDefaultUser("admin", "adminpassword");
            }
            else
            {
                _userIdCount = storedUsers.IdCount;
                List<User> usersFromFile = storedUsers.ListToPersist;
                _applicationUsers = usersFromFile;
            }
        }

        public static UserService Instance { get { return _instance; } }

        /// <summary>
        /// Currently logged in user
        /// </summary>
        public User CurrentUser { get; private set; }

        public List<User> AllUsers { get { return _applicationUsers; } }

        private void SaveToFile()
        {
            _fileDB.Write(UsersStoreFileName, new PersistanceWrapper<User>() { IdCount = _userIdCount, ListToPersist = _applicationUsers });
        }

        private void CreateDefaultUser(string username, string password)
        {
            CreateUser(username, password, "", "", UserRole.Admin);
        }

        /// <summary>
        /// Creates a new user
        /// </summary>
        /// <param name="name">The name of the new User</param>
        /// <returns>True if user created otherwise false </returns>
        internal void CreateUser(   string username,
                                    string password,
                                    string firstName,
                                    string lastName,
                                    UserRole role)
        {
            int newUniqueId = ++_userIdCount;

            DateTime now = DateTime.Now;

            _applicationUsers.Add(new()
            {
                Id = newUniqueId,
                FirstName = firstName,
                LastName = lastName,
                Username = username,
                Password = password,
                Role = role,
                CreatedAt = now,
                CreatedBy = CurrentUser != null ? CurrentUser.Id : newUniqueId,
                LastChangedAt = now,
                LastChangedBy = CurrentUser != null ? CurrentUser.Id : newUniqueId
            });

            SaveToFile();
        }

        internal void DeleteUser(int id)
        {
            _applicationUsers.RemoveAll(u => u.Id == id);
            SaveToFile();
        }

        internal bool IsUsernameUsed(string username)
        {
            return _applicationUsers.Exists(u => u.Username == username);
        }

        /// <summary>
        /// Finds, updates and saves the new users data. It also updates the LastChangedAt and LastChangedBy.
        /// </summary>
        /// <param name="id">The Id of the user we want to update</param>
        /// <param name="username">The new username for that user</param>
        /// <param name="password">The new password for that user</param>
        /// <param name="firstName">The new first name for that user</param>
        /// <param name="lastName">The new last name for that user</param>
        internal void UpdateUser(int id, string username, string password, string firstName, string lastName)
        {
            // we use List.First(predicate) as we have previously checked for the User existence.
            var user = _applicationUsers.First(u => u.Id == id);

            user.Username = username;
            user.Password = password;
            user.FirstName = firstName;
            user.LastName = lastName;
            user.LastChangedAt = DateTime.Now;
            user.LastChangedBy = CurrentUser.Id;

            SaveToFile();
        }

        internal bool IdExists(int id)
        {
            return _applicationUsers.Exists(u => u.Id == id);
        }

        /// <summary>
        /// Logs the user in the system by storing the data in the CurrentUser variable
        /// </summary>
        /// <param name="userName">The name of the user to be logged in</param>
        public void Login(string userName, string password)
        {
            CurrentUser = _applicationUsers.FirstOrDefault(u => u.Username == userName && u.Password == password);
        }

        /// <summary>
        /// Logs the user out of the system by removing the value of the CurrentUser variable
        /// </summary>
        public void LogOut()
        {
            CurrentUser = null;
        }
    }
}
