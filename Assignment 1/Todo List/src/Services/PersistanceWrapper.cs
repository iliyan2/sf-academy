﻿using System.Collections.Generic;

namespace TodoList.Services
{
    internal class PersistanceWrapper<T>
    {
        public int IdCount { get; set; }
        public List<T> ListToPersist { get; set; }
    }
}