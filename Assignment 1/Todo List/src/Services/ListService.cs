﻿using System;
using System.Collections.Generic;
using System.Linq;
using TodoList.Data;
using TodoList.Entities;

namespace TodoList.Services
{
    internal sealed class ListService
    {
        private static readonly ListService _instance = new();

        private const string ListsStoreFileName = "Lists.json";

        private readonly FileDatabase _fileDB;

        private readonly List<ToDoList> _toDoLists;

        private int _listIdCount = 0;

        static ListService()
        {
        }

        private ListService()
        {
            _fileDB = new FileDatabase();
            PersistanceWrapper<ToDoList> storedLists = _fileDB.Read<PersistanceWrapper<ToDoList>>(ListsStoreFileName);

            if (storedLists == null || storedLists.ListToPersist == null || storedLists.ListToPersist.Count == 0)
            {
                _toDoLists = new List<ToDoList>();
            }
            else
            {
                _listIdCount = storedLists.IdCount;
                _toDoLists = storedLists.ListToPersist;
            }
        }

        public static ListService Instance { get { return _instance; } }

        public List<ToDoList> AllToDoLists { get { return _toDoLists; } }

        internal bool IsTitleUsed(string listTitle)
        {
            return _toDoLists.Exists(list => list.Title == listTitle);
        }

        internal bool IdExists(int id)
        {
            return _toDoLists.Exists(list => list.Id == id);
        }

        internal void CreateList(string listTitle, int createdById)
        {
            int id = ++_listIdCount;
            DateTime now = DateTime.Now;
            ToDoList todoList = new ToDoList()
            {
                Title = listTitle,
                Id = id,
                CreatedAt = now,
                CreatedBy = createdById,
                LastChangedAt = now,
                LastChangedBy = createdById
            };

            _toDoLists.Add(todoList);

            SaveToFile();
        }

        /// <summary>
        /// Finds, updates and saves the new ToDoList data. It also updates the LastChangedAt and LastChangedBy.
        /// </summary>
        /// <param name="id">The Id of the user we want to update</param>
        /// <param name="title">The new username for that user</param>
        internal void UpdateList(int id, string title, int editedById)
        {
            // we use List.First(predicate) as we have previously checked for the ToDoList existence.
            var list = _toDoLists.First(list => list.Id == id);

            list.Title = title;
            list.LastChangedAt = DateTime.Now;
            list.LastChangedBy = editedById;

            SaveToFile();
        }

        internal void DeleteList(int id)
        {
            _toDoLists.RemoveAll(list => list.Id == id);
            SaveToFile();
        }

        private void SaveToFile()
        {
            _fileDB.Write(ListsStoreFileName, new PersistanceWrapper<ToDoList>() { IdCount = _listIdCount, ListToPersist = _toDoLists });
        }
    }
}
