﻿using System;
using System.Collections.Generic;
using System.Linq;
using TodoList.Data;
using TodoList.Entities;

namespace TodoList.Services
{
    internal sealed class TaskService
    {
        private static readonly TaskService _instance = new();

        private const string TasksStoreFileName = "Tasks.json";

        private readonly FileDatabase _fileDB;

        private readonly List<Task> _tasks;

        private int _taskIdCount = 0;

        static TaskService()
        {
        }

        private TaskService()
        {
            _fileDB = new FileDatabase();
            PersistanceWrapper<Task> storedTasks = _fileDB.Read<PersistanceWrapper<Task>>(TasksStoreFileName);

            if (storedTasks == null || storedTasks.ListToPersist == null || storedTasks.ListToPersist.Count == 0)
            {
                _tasks = new List<Task>();
            }
            else
            {
                _taskIdCount = storedTasks.IdCount;
                _tasks = storedTasks.ListToPersist;
            }
        }

        public static TaskService Instance { get { return _instance; } }

        internal bool IsTitleUsed(string taskTitle)
        {
            return _tasks.Exists(task => task.Title == taskTitle);
        }

        internal bool IdExists(int id)
        {
            return _tasks.Exists(task => task.Id == id);
        }

        internal void CreateTaskInList(int toDoListId, string taskTitle, int createdById, string description, bool isComplete)
        {
            int id = ++_taskIdCount;
            DateTime now = DateTime.Now;
            Task task = new Task()
            {
                Id = id,
                ListId = toDoListId,
                Title = taskTitle,
                Description = description,
                IsComplete = isComplete,
                CreatedAt = now,
                CreatedBy = createdById,
                LastChangedAt = now,
                LastChangedBy = createdById
            };

            _tasks.Add(task);

            SaveToFile();
        }

        internal List<Task> GetTasksForList(int listId)
        {
            return _tasks.FindAll(task => task.ListId == listId);
        }

        /// <summary>
        /// Finds, updates and saves the new Task data. It also updates the LastChangedAt and LastChangedBy.
        /// </summary>
        /// <param name="id">The Id of the task we want to update</param>
        /// <param name="title">The new title for that task</param>
        /// <param name="description">The new description of the task</param>
        /// <param name="isComplete">Yes if the task is completed or No if not</param>
        /// <param name="editedById">The Id of the User that edits the task</param>
        internal void UpdateTask(int id, string title, string description, bool isComplete, int editedById)
        {
            // we use List.First(predicate) as we have previously checked for the Task existence.
            var task = _tasks.First(t => t.Id == id);

            task.Title = title;
            task.Description = description;
            task.IsComplete = isComplete;
            task.LastChangedAt = DateTime.Now;
            task.LastChangedBy = editedById;

            SaveToFile();
        }

        internal void DeleteAllTasksForList(int listId)
        {
            _tasks.RemoveAll(task => task.ListId == listId);
            SaveToFile();
        }

        internal void DeleteTask(int id)
        {
            _tasks.RemoveAll(task => task.Id == id);
            SaveToFile();
        }

        private void SaveToFile()
        {
            _fileDB.Write(TasksStoreFileName, new PersistanceWrapper<Task>() { IdCount = _taskIdCount, ListToPersist = _tasks });
        }
    }
}
